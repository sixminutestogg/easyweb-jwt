layui.define(['table'], function (exports) {
    var setter = {
        baseServer: 'api/', // 接口地址，实际项目请换成http形式的地址
        pageTabs: true,   // 是否开启多标签
        cacheTab: false,  // 是否记忆Tab
        defaultTheme: '',  // 默认主题
        openTabCtxMenu: true,   // 是否开启Tab右键菜单
        maxTabNum: 20,  // 最多打开多少个tab
        viewPath: 'components', // 视图位置
        viewSuffix: '.html',  // 视图后缀
        reqPutToPost: false,  // req请求put方法变成post
        apiNoCache: true,      // ajax请求json数据不缓存
        tableName: 'easyweb-jwt',  // 存储表名
        // 获取缓存的token
        getToken: function () {
            var cache = layui.data(setter.tableName);
            return cache ? cache.token : undefined;
        },
        // 清除token
        removeToken: function () {
            layui.data(setter.tableName, {key: 'token', remove: true});
        },
        // 缓存token
        putToken: function (token) {
            layui.data(setter.tableName, {key: 'token', value: token});
        },
        // 当前登录的用户
        getUser: function () {
            var cache = layui.data(setter.tableName);
            return cache ? cache.loginUser : undefined;
        },
        // 缓存user
        putUser: function (user) {
            layui.data(setter.tableName, {key: 'loginUser', value: user});
        },
        // 获取用户所有权限
        getUserAuths: function () {
            var auths = [];
            var authorities = config.getUser().authorities;
            for (var i = 0; i < authorities.length; i++) {
                auths.push(authorities[i].authority);
            }
            return auths;
        },
        // ajax请求的header
        getAjaxHeaders: function () {
            var headers = [];
            var token = setter.getToken();
            if (token) {
                headers.push({
                    name: 'Authorization',
                    value: token.token_type + ' ' + token.access_token
                });
            }
            return headers;
        },
        // ajax请求结束后的处理，返回false阻止代码执行
        ajaxSuccessBefore: function (res, url, obj) {
            if (401 == res.code) {
                setter.removeToken();
                layer.msg('登录过期，请重新登录', {icon: 2, anim: 6, time: 1300}, function () {
                    location.replace('login.html');
                });
                return false;
            }
            return true;
        },
        // 路由不存在处理
        routerNotFound: function (r) {
            layui.admin.alert('地址' + r.href + '不存在', {
                title: '提示',
                btn: [],
                offset: '30px',
                anim: 6,
                shadeClose: true
            });
        }
    };
    // 表格自动传递header
    var token = setter.getToken();
    if (token && token.access_token) {
        layui.table.set({
            headers: {
                'Authorization': token.token_type + ' ' + token.access_token
            }
        });
    }
    exports('setter', setter);
});
