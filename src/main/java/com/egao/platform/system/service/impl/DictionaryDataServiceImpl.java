package com.egao.platform.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.egao.platform.common.core.PageParam;
import com.egao.platform.common.core.PageResult;
import com.egao.platform.system.mapper.DictionaryDataMapper;
import com.egao.platform.system.entity.DictionaryData;
import com.egao.platform.system.service.DictionaryDataService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 字典项服务实现类
 * Created by wangfan on 2020-03-14 11:29:04
 */
@Service
public class DictionaryDataServiceImpl extends ServiceImpl<DictionaryDataMapper, DictionaryData> implements DictionaryDataService {

    @Override
    public PageResult<DictionaryData> listPage(PageParam page) {
        List<DictionaryData> records = baseMapper.listPage(page);
        return new PageResult<>(records, page.getTotal());
    }

    @Override
    public List<DictionaryData> listAll(Map page) {
        return baseMapper.listAll(page);
    }

    @Override
    public List<DictionaryData> listByDictCode(String dictCode) {
        PageParam<DictionaryData> pageParam = new PageParam<>();
        pageParam.put("dictCode", dictCode).setDefaultOrder(new String[]{"sort_number"}, null);
        List<DictionaryData> records = baseMapper.listAll(pageParam.getNoPageParam());
        return pageParam.sortRecords(records);
    }

}
