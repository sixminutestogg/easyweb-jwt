package com.egao.platform.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.egao.platform.system.entity.Authorities;

import java.util.List;

/**
 * 权限服务类
 * Created by wangfan on 2018-12-24 16:10
 */
public interface AuthoritiesService extends IService<Authorities> {

    /**
     * 查询角色权限列表
     */
    List<String> listByRoleId(Integer roleId);

    /**
     * 查询角色权限列表
     */
    List<String> listByRoleIds(List<Integer> roleIds);

    /**
     * 查询用户权限列表
     */
    List<String> listByUserId(Integer userId);

    /**
     * 回显出角色授权的选中状态
     */
    List<Authorities> selectRoleChecked(List<Authorities> authorities, Integer roleId);

    /**
     * 自动扫描接口生成权限集合
     */
    List<Authorities> scan();

}
