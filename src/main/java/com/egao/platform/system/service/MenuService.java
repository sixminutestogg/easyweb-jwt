package com.egao.platform.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.egao.platform.common.core.PageParam;
import com.egao.platform.common.core.PageResult;
import com.egao.platform.system.entity.Menu;

import java.util.List;
import java.util.Map;

/**
 * 菜单服务类
 * Created by wangfan on 2018-12-24 16:10
 */
public interface MenuService extends IService<Menu> {

    /**
     * 关联分页查询菜单
     */
    PageResult<Menu> listPage(PageParam pageParam);

    /**
     * 根据用户id查询菜单列表
     */
    List<Menu> getUserMenu(Integer userId);

    /**
     * 根据权限集合查询菜单列表
     */
    List<Menu> getUserMenu(List<String> authority);

    /**
     * 递归转化树形结构
     *
     * @param menus    菜单list
     * @param parentId 最顶级id
     * @return List<Map < String, Object>>
     */
    List<Map<String, Object>> toMenuTree(List<Menu> menus, Integer parentId);

    /**
     * 递归转化树形结构
     *
     * @param menus     菜单list
     * @param parentId  最顶级id
     * @param menuName  菜单名称字段名
     * @param menuIcon  菜单图标字段名
     * @param menuUrl   菜单url(hash地址)字段名
     * @param iframeUrl iframe嵌入地址字段名
     * @param target    菜单打开位置字段名
     * @param showSide  菜单是否显示在侧边栏字段名
     * @param subMenus  子菜单字段名
     * @return List<Map < String, Object>>
     */
    List<Map<String, Object>> toMenuTree(
            List<Menu> menus, Integer parentId,
            String menuName, String menuIcon, String menuUrl,
            String iframeUrl, String target, String showSide, String subMenus);

}
