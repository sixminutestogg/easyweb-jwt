package com.egao.platform.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.egao.platform.common.core.PageParam;
import com.egao.platform.common.core.PageResult;
import com.egao.platform.system.entity.LoginRecord;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * 登录日志服务类
 * Created by wangfan on 2018-12-24 16:10
 */
public interface LoginRecordService extends IService<LoginRecord> {

    /**
     * 关联分页查询
     */
    PageResult<LoginRecord> listPage(PageParam page);

    /**
     * 关联查询所有
     */
    List<LoginRecord> listAll(Map page);

    /**
     * 添加登录日志
     *
     * @param userId   用户id
     * @param type     操作类型
     * @param comments 备注
     * @param request  HttpServletRequest
     * @return boolean
     */
    boolean add(Integer userId, Integer type, String comments, HttpServletRequest request);

}
