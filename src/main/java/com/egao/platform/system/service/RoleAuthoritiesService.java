package com.egao.platform.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.egao.platform.system.entity.RoleAuthorities;

/**
 * 角色权限服务类
 * Created by wangfan on 2018-12-24 16:10
 */
public interface RoleAuthoritiesService extends IService<RoleAuthorities> {

    /**
     * 删除垃圾数据(对应权限已被删除的)
     */
    void deleteTrash();

}
