package com.egao.platform.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.egao.platform.system.mapper.RoleAuthoritiesMapper;
import com.egao.platform.system.entity.RoleAuthorities;
import com.egao.platform.system.service.RoleAuthoritiesService;
import org.springframework.stereotype.Service;

/**
 * 角色权限服务实现类
 * Created by wangfan on 2018-12-24 16:10
 */
@Service
public class RoleAuthoritiesServiceImpl extends ServiceImpl<RoleAuthoritiesMapper, RoleAuthorities> implements RoleAuthoritiesService {

    @Override
    public void deleteTrash() {
        baseMapper.deleteTrash();
    }

}
