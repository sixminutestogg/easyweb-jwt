package com.egao.platform.system.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.egao.platform.common.core.PageParam;
import com.egao.platform.common.core.PageResult;
import com.egao.platform.system.mapper.AuthoritiesMapper;
import com.egao.platform.system.mapper.MenuMapper;
import com.egao.platform.system.entity.Menu;
import com.egao.platform.system.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 菜单服务实现类
 * Created by wangfan on 2018-12-24 16:10
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements MenuService {
    @Autowired
    private AuthoritiesMapper authoritiesMapper;

    @Override
    public PageResult<Menu> listPage(PageParam pageParam) {
        return new PageResult<>(baseMapper.listPage(pageParam), pageParam.getTotal());
    }

    @Override
    public List<Menu> getUserMenu(Integer userId) {
        return getUserMenu(authoritiesMapper.listByUserId(userId));
    }

    @Override
    public List<Menu> getUserMenu(List<String> authority) {
        // 查询所有的菜单
        List<Menu> menus = baseMapper.selectList(new QueryWrapper<Menu>().orderByAsc("sort_number"));
        // 移除没有权限的菜单
        Iterator<Menu> iterator = menus.iterator();
        while (iterator.hasNext()) {
            String menuAuth = iterator.next().getAuthority();
            if (StrUtil.isNotBlank(menuAuth) && (authority == null || !authority.contains(menuAuth))) {
                iterator.remove();
            }
        }
        // 去除空的目录
        iterator = menus.iterator();
        while (iterator.hasNext()) {
            Menu menu = iterator.next();
            if (StrUtil.isBlank(menu.getMenuUrl())) {
                boolean haveSub = false;
                for (Menu m : menus) {
                    if (m.getParentId().equals(menu.getMenuId())) {
                        haveSub = true;
                        break;
                    }
                }
                if (!haveSub) {
                    iterator.remove();
                }
            }
        }
        return menus;
    }

    public List<Map<String, Object>> toMenuTree(List<Menu> menus, Integer parentId) {
        return toMenuTree(menus, parentId,
                "name", "icon", "url",
                "iframe", "target", "show", "subMenus");
    }

    public List<Map<String, Object>> toMenuTree(
            List<Menu> menus, Integer parentId,
            String menuName, String menuIcon, String menuUrl,
            String iframeUrl, String target, String showSide, String subMenus) {
        List<Map<String, Object>> list = new ArrayList<>();
        for (int i = 0; i < menus.size(); i++) {
            Menu temp = menus.get(i);
            if (parentId.equals(temp.getParentId())) {
                Map<String, Object> map = new HashMap<>();
                map.put(menuName, temp.getMenuName());
                map.put(menuIcon, temp.getMenuIcon());
                map.put(menuUrl, temp.getMenuUrl());
                map.put(iframeUrl, temp.getIframeUrl());
                map.put(target, temp.getTarget());
                map.put(showSide, temp.getShowSide().equals(1) ? true : 0);
                map.put(subMenus, toMenuTree(
                        menus, menus.get(i).getMenuId(),
                        menuName, menuIcon, menuUrl,
                        iframeUrl, target, showSide, subMenus));
                list.add(map);
            }
        }
        return list;
    }

}
