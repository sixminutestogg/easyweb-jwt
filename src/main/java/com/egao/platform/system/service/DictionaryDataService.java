package com.egao.platform.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.egao.platform.common.core.PageParam;
import com.egao.platform.common.core.PageResult;
import com.egao.platform.system.entity.DictionaryData;

import java.util.List;
import java.util.Map;

/**
 * 字典项服务类
 * Created by wangfan on 2020-03-14 11:29:04
 */
public interface DictionaryDataService extends IService<DictionaryData> {

    /**
     * 关联分页查询
     */
    PageResult<DictionaryData> listPage(PageParam page);

    /**
     * 关联查询所有
     */
    List<DictionaryData> listAll(Map page);

    /**
     * 根据字典代码查询字典项
     */
    List<DictionaryData> listByDictCode(String dictCode);
}
