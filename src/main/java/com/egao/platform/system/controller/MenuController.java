package com.egao.platform.system.controller;

import com.egao.platform.common.core.*;
import com.egao.platform.common.annotation.ApiPageParam;
import com.egao.platform.system.entity.Menu;
import com.egao.platform.system.service.MenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by wangfan on 2018-12-24 16:10
 */
@Api(tags = "菜单管理")
@RestController
@RequestMapping("/api/sys/menu")
public class MenuController extends BaseController {
    @Autowired
    private MenuService menuService;

    @ApiOperation("分页查询菜单")
    @ApiPageParam
    @GetMapping("/page")
    public PageResult<Menu> page(HttpServletRequest request) {
        PageParam<Menu> pageParam = new PageParam<>(request);
        pageParam.setDefaultOrder(new String[]{"sort_number"}, null);
        return menuService.listPage(pageParam);
    }

    @ApiOperation("查询全部菜单")
    @GetMapping()
    public JsonResult list(HttpServletRequest request) {
        PageParam<Menu> pageParam = new PageParam<>(request);
        pageParam.setDefaultOrder(new String[]{"sort_number"}, null);
        return JsonResult.ok().setData(menuService.list(pageParam.getOrderWrapper()));
    }

    @ApiOperation("根据id查询菜单")
    @GetMapping("/{id}")
    public JsonResult get(@PathVariable("id") Integer id) {
        return JsonResult.ok().setData(menuService.getById(id));
    }

    @ApiOperation("添加菜单")
    @PostMapping()
    public JsonResult add(@RequestBody Menu menu) {
        if (menuService.save(menu)) {
            return JsonResult.ok("添加成功");
        }
        return JsonResult.error("添加失败");
    }

    @ApiOperation("修改菜单")
    @PutMapping()
    public JsonResult update(@RequestBody Menu menu) {
        if (menuService.updateById(menu)) {
            return JsonResult.ok("修改成功");
        }
        return JsonResult.error("修改失败");
    }

    @ApiOperation("删除菜单")
    @DeleteMapping("/{id}")
    public JsonResult delete(@PathVariable("id") Integer id) {
        if (menuService.removeById(id)) {
            return JsonResult.ok("删除成功");
        }
        return JsonResult.error("删除失败");
    }

    @ApiOperation("批量添加菜单")
    @PostMapping("/batch")
    public JsonResult saveBatch(@RequestBody List<Menu> menuList) {
        if (menuService.saveBatch(menuList)) {
            return JsonResult.ok("添加成功");
        }
        return JsonResult.error("添加失败");
    }

    @ApiOperation("批量修改菜单")
    @PutMapping("/batch")
    public JsonResult updateBatch(@RequestBody BatchParam batchParam) {
        if (batchParam.update(menuService, "menu_id", Menu.class)) {
            return JsonResult.ok("修改成功");
        }
        return JsonResult.error("修改失败");
    }

    @ApiOperation("批量删除菜单")
    @DeleteMapping("/batch")
    public JsonResult deleteBatch(@RequestBody List<Integer> ids) {
        if (menuService.removeByIds(ids)) {
            return JsonResult.ok("删除成功");
        }
        return JsonResult.error("删除失败");
    }

}
