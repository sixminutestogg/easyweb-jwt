package com.egao.platform.system.controller;

import com.egao.platform.common.annotation.ApiPageParam;
import com.egao.platform.common.core.BaseController;
import com.egao.platform.common.core.JsonResult;
import com.egao.platform.common.core.PageParam;
import com.egao.platform.common.core.PageResult;
import com.egao.platform.system.entity.LoginRecord;
import com.egao.platform.system.service.LoginRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by wangfan on 2018-12-24 16:10
 */
@Api(tags = "登录日志")
@RestController
@RequestMapping("/api/sys/loginRecord")
public class LoginRecordController extends BaseController {
    @Autowired
    private LoginRecordService loginRecordService;

    @ApiOperation("分页查询登录日志")
    @ApiPageParam
    @GetMapping("/page")
    public PageResult<LoginRecord> page(HttpServletRequest request) {
        PageParam<LoginRecord> pageParam = new PageParam<>(request);
        pageParam.setDefaultOrder(null, new String[]{"create_time"});
        return loginRecordService.listPage(pageParam);
    }

    @ApiOperation("查询全部登录日志")
    @GetMapping()
    public JsonResult list(HttpServletRequest request) {
        PageParam<LoginRecord> pageParam = new PageParam<>(request);
        List<LoginRecord> records = loginRecordService.listAll(pageParam.getNoPageParam());
        return JsonResult.ok().setData(pageParam.sortRecords(records));
    }

    @ApiOperation("根据id查询登录日志")
    @GetMapping("/{id}")
    public JsonResult get(@PathVariable("id") Integer id) {
        PageParam<LoginRecord> pageParam = new PageParam<>();
        pageParam.put("id", id);
        List<LoginRecord> records = loginRecordService.listAll(pageParam.getNoPageParam());
        return JsonResult.ok().setData(pageParam.getOne(records));
    }

}
