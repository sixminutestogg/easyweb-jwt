package com.egao.platform.system.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.egao.platform.common.annotation.ApiPageParam;
import com.egao.platform.common.core.*;
import com.egao.platform.system.entity.Organization;
import com.egao.platform.system.service.OrganizationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by AutoGenerator on 2020-03-14 11:29:04
 */
@Api(tags = "组织机构管理")
@RestController
@RequestMapping("/api/sys/organization")
public class OrganizationController extends BaseController {
    @Autowired
    private OrganizationService organizationService;

    @ApiOperation("分页查询组织机构")
    @ApiPageParam
    @GetMapping("/page")
    public PageResult<Organization> page(HttpServletRequest request) {
        PageParam<Organization> pageParam = new PageParam<>(request);
        return organizationService.listPage(pageParam);
    }

    @ApiOperation("查询全部组织机构")
    @GetMapping()
    public JsonResult list(HttpServletRequest request) {
        PageParam<Organization> pageParam = new PageParam<>(request);
        List<Organization> records = organizationService.listAll(pageParam.getNoPageParam());
        return JsonResult.ok().setData(pageParam.sortRecords(records));
    }

    @ApiOperation("根据id查询组织机构")
    @GetMapping("/{id}")
    public JsonResult get(@PathVariable("id") Integer id) {
        PageParam<Organization> pageParam = new PageParam<>();
        pageParam.put("organizationId", id);
        List<Organization> records = organizationService.listAll(pageParam.getNoPageParam());
        return JsonResult.ok().setData(pageParam.getOne(records));
    }

    @ApiOperation("添加组织机构")
    @PostMapping()
    public JsonResult add(@RequestBody Organization organization) {
        if (organization.getParentId() == null) organization.setParentId(0);
        if (organizationService.count(new QueryWrapper<Organization>()
                .eq("organization_name", organization.getOrganizationName())
                .eq("parent_id", organization.getParentId())) > 0) {
            return JsonResult.error("机构名称已存在");
        }
        if (organizationService.save(organization)) {
            return JsonResult.ok("添加成功");
        }
        return JsonResult.error("添加失败");
    }

    @ApiOperation("修改组织机构")
    @PutMapping()
    public JsonResult update(@RequestBody Organization organization) {
        if (organization.getParentId() == null) organization.setParentId(0);
        if (organizationService.count(new QueryWrapper<Organization>()
                .eq("organization_name", organization.getOrganizationName())
                .eq("parent_id", organization.getParentId())
                .ne("organization_id", organization.getOrganizationId())) > 0) {
            return JsonResult.error("机构名称已存在");
        }
        if (organizationService.updateById(organization)) {
            return JsonResult.ok("修改成功");
        }
        return JsonResult.error("修改失败");
    }

    @ApiOperation("删除组织机构")
    @DeleteMapping("/{id}")
    public JsonResult delete(@PathVariable("id") Integer id) {
        if (organizationService.removeById(id)) {
            return JsonResult.ok("删除成功");
        }
        return JsonResult.error("删除失败");
    }

    @ApiOperation("批量添加组织机构")
    @PostMapping("/batch")
    public JsonResult saveBatch(@RequestBody List<Organization> organizationList) {
        if (organizationService.saveBatch(organizationList)) {
            return JsonResult.ok("添加成功");
        }
        return JsonResult.error("添加失败");
    }

    @ApiOperation("批量修改组织机构")
    @PutMapping("/batch")
    public JsonResult updateBatch(@RequestBody BatchParam batchParam) {
        if (batchParam.update(organizationService, "organization_id", Organization.class)) {
            return JsonResult.ok("修改成功");
        }
        return JsonResult.error("修改失败");
    }

    @ApiOperation("批量删除组织机构")
    @DeleteMapping("/batch")
    public JsonResult deleteBatch(@RequestBody List<Integer> ids) {
        if (organizationService.removeByIds(ids)) {
            return JsonResult.ok("删除成功");
        }
        return JsonResult.error("删除失败");
    }

}
