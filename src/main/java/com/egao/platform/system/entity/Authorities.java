package com.egao.platform.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by wangfan on 2018-12-24 16:10
 */
@ApiModel(description = "权限")
@TableName("sys_authorities")
public class Authorities implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("授权标识")
    @TableId(value = "authority", type = IdType.INPUT)
    private String authority;

    @ApiModelProperty("权限名称")
    private String authorityName;

    @ApiModelProperty("模块名称")
    private String parentName;

    @ApiModelProperty("排序号")
    private Integer sortNumber;

    @ApiModelProperty("创建时间")
    private Date createTime;

    @ApiModelProperty("回显选中状态,0未选中,1选中")
    @TableField(exist = false)
    private int checked;

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public String getAuthorityName() {
        return authorityName;
    }

    public void setAuthorityName(String authorityName) {
        this.authorityName = authorityName;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public Integer getSortNumber() {
        return sortNumber;
    }

    public void setSortNumber(Integer sortNumber) {
        this.sortNumber = sortNumber;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public int getChecked() {
        return checked;
    }

    public void setChecked(int checked) {
        this.checked = checked;
    }

    @Override
    public String toString() {
        return "Authorities{" +
                ", authority=" + authority +
                ", authorityName=" + authorityName +
                ", parentName=" + parentName +
                ", sortNumber=" + sortNumber +
                ", createTime=" + createTime +
                ", checked=" + checked +
                "}";
    }
}
