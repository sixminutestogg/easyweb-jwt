package com.egao.platform.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.egao.platform.system.entity.RoleAuthorities;

/**
 * 角色权限Mapper接口
 * Created by wangfan on 2018-12-24 16:10
 */
public interface RoleAuthoritiesMapper extends BaseMapper<RoleAuthorities> {

    /**
     * 删除垃圾数据(未关联权限的)
     */
    int deleteTrash();

}
