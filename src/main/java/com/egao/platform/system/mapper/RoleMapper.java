package com.egao.platform.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.egao.platform.system.entity.Role;

/**
 * 角色Mapper接口
 * Created by wangfan on 2018-12-24 16:10
 */
public interface RoleMapper extends BaseMapper<Role> {

}
