package com.egao.platform.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.egao.platform.common.core.PageParam;
import com.egao.platform.system.entity.Menu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 菜单Mapper接口
 * Created by wangfan on 2018-12-24 16:10
 */
public interface MenuMapper extends BaseMapper<Menu> {

    /**
     * 分页查询
     */
    List<Menu> listPage(@Param("page") PageParam page);

}
