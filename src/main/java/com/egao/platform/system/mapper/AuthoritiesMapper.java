package com.egao.platform.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.egao.platform.system.entity.Authorities;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 权限Mapper接口
 * Created by wangfan on 2018-12-24 16:10
 */
public interface AuthoritiesMapper extends BaseMapper<Authorities> {

    /**
     * 查询角色权限列表
     */
    List<String> listByRoleIds(@Param("roleIds") List<Integer> roleIds);

    /**
     * 查询用户权限列表
     */
    List<String> listByUserId(@Param("userId") Integer userId);

    /**
     * 查询用户权限列表
     */
    List<Authorities> listAuthByUserId(@Param("userId") Integer userId);

}
