package com.egao.platform.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.egao.platform.common.core.PageParam;
import com.egao.platform.system.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 用户Mapper接口
 * Created by wangfan on 2018-12-24 16:10
 */
public interface UserMapper extends BaseMapper<User> {

    /**
     * 分页查询
     */
    List<User> listPage(@Param("page") PageParam page);

    /**
     * 查询全部
     */
    List<User> listAll(@Param("page") Map page);

}
