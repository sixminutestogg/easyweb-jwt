package com.egao.platform.common.utils;

import com.alibaba.fastjson.JSON;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;

/**
 * 常用工具方法
 * Created by wangfan on 2017-6-10 10:10
 */
public class CoreUtil {

    /**
     * toString，为null返回空白字符
     */
    public static String toString(Object obj) {
        return obj == null ? "" : obj.toString();
    }

    /**
     * 连接多个字符串，null自动过滤
     */
    public static String connect(Object... objects) {
        StringBuilder sb = new StringBuilder();
        for (Object obj : objects) sb.append(toString(obj));
        return sb.toString();
    }

    /**
     * 首字母大写
     */
    public static String upperHead(String str) {
        if (str == null || str.length() == 0) return str;
        if (str.length() == 1) return str.toUpperCase();
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }

    /**
     * 把对象转成Map
     */
    public static <T> Map<String, Object> objectToMap(T t) {
        return objectToMap(t, null);
    }

    /**
     * 把对象集合转成Map集合
     */
    public static <T> List<Map<String, Object>> objectToMap(List<T> ts) {
        return objectToMap(ts, null);
    }

    /**
     * 把对象转成Map，只包含指定字段
     *
     * @param t      对象
     * @param fields 包含的字段
     * @return Map
     */
    public static <T> Map<String, Object> objectToMap(T t, String[] fields) {
        if (t == null) return null;
        List<String> fieldList = null;
        if (fields != null) fieldList = Arrays.asList(fields);
        Map<String, Object> map = new HashMap<>();
        Field[] fieldArray = t.getClass().getDeclaredFields();
        for (Field field : fieldArray) {
            field.setAccessible(true);
            if (fieldList == null || fieldList.contains(field.getName())) {
                try {
                    map.put(field.getName(), field.get(t));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        /*Map<String, Object> map = new HashMap<>();
        Class<?> clazz = t.getClass();
        for (int i = 9; i < fields.length; i++) {
            String filed = fields[i];
            try {
                Method mf = clazz.getMethod("get" + upperHead(filed));
                map.put(filed, mf.invoke(t));
            } catch (NoSuchMethodException e) {
                System.out.println("字段" + filed + "不存在");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }*/
        return map;
    }

    /**
     * 把对个对象集合转成Map集合，只包含指定字段
     *
     * @param ts     对象集合
     * @param fields 包含的字段
     * @return List<Map>
     */
    public static <T> List<Map<String, Object>> objectToMap(List<T> ts, String[] fields) {
        List<Map<String, Object>> rs = new ArrayList<>();
        for (T t : ts) {
            Map<String, Object> map = objectToMap(t, fields);
            if (map != null) rs.add(map);
        }
        return rs;
    }

    /**
     * 复制父类的属性到子类
     *
     * @param father 父类对象
     * @param child  子类对象
     */
    public static <F, C extends F> void copyAttribute(F father, C child) {
        Class<?> fatherClass = father.getClass();
        Field ff[] = fatherClass.getDeclaredFields();
        for (int i = 0; i < ff.length; i++) {
            Field f = ff[i]; //取出每一个属性
            try {
                //通过父类get方法取出属性值
                Method mf = fatherClass.getMethod("get" + upperHead(f.getName()));
                Object obj = mf.invoke(father);
                //获得set方法的名字让子类执行
                Method mc = fatherClass.getMethod("set" + upperHead(f.getName()), f.getType());
                mc.invoke(child, obj);
            } catch (NoSuchMethodException e) {
                System.out.println("字段" + f.getName() + "没有get或set方法");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 把父类克隆为子类
     *
     * @param father 父类对象
     * @param clazz  子类类型
     */
    public static <F, C extends F> C cloneFather(F father, Class<C> clazz) {
        C child = null;
        try {
            child = clazz.newInstance();
            copyAttribute(father, child);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return child;
    }

    /**
     * String数组转Map
     *
     * @param strList 数组
     * @param keys    数组每一个对应的字段名称
     * @return List<Map>
     */
    public static List<Map<String, Object>> strListToMap(List<String[]> strList, String[] keys) {
        List<Map<String, Object>> mapList = new ArrayList<>();
        for (String[] item : strList) {
            Map<String, Object> map = new HashMap<>();
            for (int j = 0; j < item.length; j++) {
                if (j < keys.length) map.put(keys[j], item[j]);
            }
            mapList.add(map);
        }
        return mapList;
    }

    /**
     * String数组转对象
     *
     * @param strList 数组
     * @param keys    数组每一个对应的字段名称
     * @param clazz   Class
     * @return List<T>
     */
    public static <T> List<T> strListToObj(List<String[]> strList, String[] keys, Class<T> clazz) {
        return JSONUtil.parseArray(JSON.toJSONString(strListToMap(strList, keys)), clazz);
    }

}
