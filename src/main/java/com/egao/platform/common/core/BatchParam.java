package com.egao.platform.common.core;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by wangfan on 2020-03-13 0:11
 */
@ApiModel(description = "批量修改通用参数")
public class BatchParam implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键id集合")
    private List<Integer> ids;

    @ApiModelProperty("需要修改的字段名称")
    private String field;

    @ApiModelProperty("修改后的值")
    private Object value;

    public List<Integer> getIds() {
        return ids;
    }

    public void setIds(List<Integer> ids) {
        this.ids = ids;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    /**
     * 使用反射生成修改的对象
     *
     * @param clazz 对象类型
     * @return T
     */
    public <T> T getObject(Class<T> clazz) {
        T obj = null;
        try {
            obj = clazz.newInstance();
            Field clazzField = clazz.getDeclaredField(this.field);
            clazzField.setAccessible(true);
            clazzField.set(obj, value);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return obj;
    }

    /**
     * 通用批量修改方法
     *
     * @param service IService
     * @param idName  id字段名称
     * @param clazz   实体类型
     * @return boolean
     */
    public <T> boolean update(IService<T> service, String idName, Class<T> clazz) {
        return service.update(getObject(clazz), new UpdateWrapper<T>().in(idName, this.getIds()));
    }

    /**
     * value获取为常用类型
     */
    public String getString() {
        return value == null ? null : String.valueOf(value);
    }

    public Integer getInteger() {
        try {
            return value == null ? null : Integer.parseInt(getString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Double getDouble() {
        try {
            return value == null ? null : Double.parseDouble(getString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Float getFloat() {
        try {
            return value == null ? null : Float.parseFloat(getString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Boolean getBoolean() {
        try {
            return value == null ? null : Boolean.parseBoolean(getString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * value获取为Date，智能判断格式
     *
     * @return Date
     */
    public Date getDate() {
        String str = getString();
        if (str == null || str.trim().isEmpty()) {
            return null;
        } else if (str.matches("^\\d{4}-\\d{1,2}-\\d{1,2}$")) {
            return getDate("yyyy-MM-dd");
        } else if (str.matches("^\\d{4}-\\d{1,2}-\\d{1,2} {1}\\d{1,2}:\\d{1,2}$")) {
            return getDate("yyyy-MM-dd HH:mm");
        } else if (str.matches("^\\d{4}-\\d{1,2}-\\d{1,2} {1}\\d{1,2}:\\d{1,2}:\\d{1,2}$")) {
            return getDate("yyyy-MM-dd HH:mm:ss");
        }
        return null;
    }

    /**
     * value获取为Date
     *
     * @param format 日期格式
     * @return Date
     */
    public Date getDate(String format) {
        if (value == null) return null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            return sdf.parse(getString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
